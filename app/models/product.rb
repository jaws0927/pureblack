class Product < ActiveRecord::Base
	belongs_to :cafe
	has_and_belongs_to_many :users
	has_many :payinfos, dependent: :destroy
end

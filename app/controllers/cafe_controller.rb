class CafeController < ApplicationController
	
  CafeCreaterTag = 0;
  UserTag = 1;

  def cafesearch
    cafe_reorder = 0
    if params[:cafe_reorder].present?
      cafe_reorder = params[:cafe_reorder].to_i
    end
    
    @search = params[:searchword]
    tags = Tag.where(:content => params[:searchword])
    tags.each do |tag|
      tag.increment!(:counting)
    end
    prev = params[:searchword]
    tags += Tag.where("content LIKE ? AND content != ?", "%"+params[:searchword]+"%", params[:searchword])

    overlap_caves = Array.new

    for i in 0.. tags.size-1
      overlap_caves+=tags[i].caves
    end

    coffee = overlap_caves.uniq.sort
    cafetags = Array.new
   
    for i in 0..coffee.size-1
      cafetags+=coffee[i].tags.where.not(:content=> params[:searchword]).where.not(:content=>"")
    end

    if cafe_reorder == 1
      @cafe = coffee.sort {|a, b| a.posts.count <=> b.posts.count }.reverse!
    else
      @cafe = coffee.sort {|a, b| a.avg <=> b.avg }.reverse!
    end
    if cafe_reorder == 2
      if params[:meal_available] == "true"
        @cafe.delete_if{|x| x.meal_available != true}
      end
      if params[:parking] == "true"
        @cafe.delete_if{|x| x.parking != true}
      end
      if params[:wifi] == "true"
        @cafe.delete_if{|x| x.wifi != true}
      end
      if params[:sidemenu] == "true"
        @cafe.delete_if{|x| x.sidemenu != true}
      end
      if params[:animal_available] == "true"
        @cafe.delete_if{|x| x.animal_available != true}
      end
      if params[:in_house_roasting] == "true"
        @cafe.delete_if{|x| x.in_house_roasting != true}
      end
      if params[:smoke] == "true"
        @cafe.delete_if{|x| x.smoke != true}
      end
      if params[:today_business_check] == "true"
        @cafe.delete_if{|x| x.today_business_check != true}
      end
    end
    @aa=cafetags.sort_by {|m| m[:counting]}.reverse!
    @hashtags = @aa.collect(&:content).uniq
    @navsearch = params[:searchword] #application_html 의 #navbar에서 사용
  end  

  def cafedetail
    if current_user
      @cafe = Cafe.where(:cafehash => params[:hash]).take
    else
      redirect_to '/users/sign_in'
    end
  end

  def cafe_edit
    edit = Cafe.where(:id => params[:id]).take
    edit.thumnail = params[:thumbnail]
    edit.image = params[:cafeimage]
    edit.signaturemenu = params[:signaturemenu] 
    edit.cafecontact = params[:cafecontact] 
    edit.introduction = params[:intro_detail]
    edit.webinfo_address = params[:webinfo_adderess]
    edit.facebook_address = params[:facebook_address]
    edit.instagram_adderess = params[:instagram_adderess]
    edit.blog_address = params[:blog_adderess]
    edit.machine = params[:machine]
    edit.grinder = params[:grinder]
    edit.bean = params[:beans]
    edit.brewing_method = params[:brewing_method]
    edit.business_hour = params[:opening_time]+"~"+params[:closing_time]
    edit.machine_link = params[:machine_link]
    edit.grinder_link = params[:grinder_link]
    edit.bean_link = params[:bean_link]
    edit.brewing_method_link = params[:brewing_method_link]
    if params[:meal_available] == 'checked'
      edit.meal_available = true
    else
      edit.meal_available = false
    end
    if params[:parking] == 'checked'
      edit.parking = true
    else
      edit.parking = false
    end
    if params[:wifi] == 'checked'
      edit.wifi = true
    else
      edit.wifi = false
    end
    if params[:sidemenu] == 'checked'
      edit.sidemenu = true
    else
      edit.sidemenu = false
    end
    if params[:animal_available] === 'checked'
      edit.animal_available = true
    else
      edit.animal_available = false
    end
    if params[:in_house_roasting] == 'checked'
      edit.in_house_roasting  = true
    else
      edit.in_house_roasting = false
    end
    if params[:today_business_check] == 'checked'
      edit.today_business_check = true
    else
      edit.today_business_check = false
    end
    if params[:smoke] == 'checked'
      edit.smoke = true
    else
      edit.smoke = false
    end
    edit.save
    tag1 = Tag.where(:content=>params[:tag1]).take
    caves_tags1 = CavesTag.where(:cafe_id=> edit.id, :tag_id=> edit.tags[3].id).take
    tag2 = Tag.where(:content=>params[:tag2]).take
    caves_tags2 = CavesTag.where(:cafe_id=> edit.id, :tag_id=> edit.tags[4].id).take
    tag3 = Tag.where(:content=>params[:tag3]).take
    caves_tags3 = CavesTag.where(:cafe_id=> edit.id, :tag_id=> edit.tags[5].id).take
    if tag1.nil?
      tag = Tag.new
      tag.content = params[:tag1]
      tag.save
      caves_tags1.tag_id = tag.id
      caves_tags1.save
    else
      caves_tags1.tag_id = tag1.id
      caves_tags1.save
    end
    if tag2.nil?
      tag = Tag.new
      tag.content = params[:tag2]
      tag.save
      caves_tags2.tag_id = tag.id
      caves_tags2.save
    else
      caves_tags2.tag_id = tag2.id
      caves_tags2.save
    end
    if tag3.nil?
      tag = Tag.new
      tag.content = params[:tag3]
      tag.save
      caves_tags3.tag_id = tag.id
      caves_tags3.save
    else
      caves_tags3.tag_id = tag3.id
      caves_tags3.save
    end
    
    redirect_to "/cafe/cafedetail?hash=#{edit.cafehash}"
  end

  def cafecreate

    cafe = Cafe.new
    cafe.name = params[:cafename]
    cafe.thumnail = params[:thumbnail]
    cafe.image = params[:cafeimage]
    cafe.signaturemenu = params[:signaturemenu]
    cafe.introduction = params[:intro_detail]
    cafe.cafehash = SecureRandom.hex(11)
    cafe.user_id = current_user.id
    cafe.address_road = params[:buyer_addr] + " " + params[:address_detail]
    cafe.address = params[:address_jibun]
    cafe.latitude = params[:latitude]
    cafe.longitude = params[:longitude]
    cafe.cafecontact = params[:cafecontact]
    cafe.webinfo_address = params[:webinfo_adderess]
    cafe.facebook_address = params[:facebook_address]
    cafe.instagram_adderess = params[:instagram_adderess]
    cafe.blog_address = params[:blog_adderess]
    cafe.machine = params[:machine]
    cafe.grinder = params[:grinder]
    cafe.bean = params[:beans]
    cafe.brewing_method = params[:brewing_method]
    cafe.business_hour = params[:opening_time]+"~"+params[:closing_time]
    cafe.machine_link = params[:machine_link]
    cafe.grinder_link = params[:grinder_link]
    cafe.bean_link = params[:bean_link]
    cafe.brewing_method_link = params[:brewing_method_link]
    if params[:meal_available] == 'checked'
      cafe.meal_available = true
    else
      cafe.meal_available = false
    end
    if params[:parking] == 'checked'
      cafe.parking = true
    else
      cafe.parking = false
    end
    if params[:wifi] == 'checked'
      cafe.wifi = true
    else
      cafe.wifi = false
    end
    if params[:sidemenu] == 'checked'
      cafe.sidemenu = true
    else
      cafe.sidemenu = false
    end
    if params[:animal_available] === 'checked'
      cafe.animal_available = true
    else
      cafe.animal_available = false
    end
    if params[:in_house_roasting] == 'checked'
      cafe.in_house_roasting  = true
    else
      cafe.in_house_roasting = false
    end
    if params[:today_business_check] == 'checked'
      cafe.today_business_check = true
    else
      cafe.today_business_check = false
    end
    if params[:smoke] == 'checked'
      cafe.smoke = true
    else
      cafe.smoke = false
    end
    cafe.save

    tags = Tag.all
    tag_name_list = Array.new
    tag_name_list.push(params[:cafename])
    tag_name_list.push(params[:signaturemenu])
    tag_name_list.push(params[:address_jibun])
    tag_name_list.push(params[:tag1])
    tag_name_list.push(params[:tag2])
    tag_name_list.push(params[:tag3])
    # cafe= Cafe.where(:name => params[:cafename]).take
    # db수정에 따른 수정 부분 -1
    # a=Tag.create(content: params[:cafename])
    # cafe.tags << a
    tag_name_list.each do |x|
        if tags.where(:content => x).take.nil?
            Tag.create(content: x)
            a=Tag.where(:content => x).take
            cafe.tags << a
        else  
            a=Tag.where(:content => x).take
            cafe.tags << a
        end  
    end

    payinfo = Payinfo.new
    payinfo.pay_method = params[:pay_method]
    payinfo.merchant_uid = params[:merchant_uid]
    payinfo.name = params[:name]
    payinfo.paid_amount = params[:paid_amount].to_i
    payinfo.pg_provider = params[:pg_provider]
    payinfo.pg_tid = params[:pg_tid]
    payinfo.apply_num = params[:apply_num]
    payinfo.vbank_num = params[:vbank_num]
    payinfo.vbank_name = params[:vbank_name]
    payinfo.vbank_holder = params[:vbank_holder]
    payinfo.vbank_date = params[:vbank_date]
    payinfo.user_id = current_user.id
    payinfo.cafe_id = Cafe.last.id
    payinfo.paid_at = DateTime.now.in_time_zone(9).strftime("%Y-%m-%d %H:%M:%S")
    payinfo.pay_status = params[:pay_status]
    payinfo.save
    
    # render :text => ""
    redirect_to "/cafe/cafedetail?hash=#{cafe.cafehash}"
    # render :js => "window.location = '/cafe/cafedetail?hash=#{cafe.cafehash}'"
  end
  
  def caferegister

  end

  def cafedelete
    CavesTag.where(:cafe_id => params[:id]).take.destroy

    post = Post.where(:cafe_id => params[:id])
    post.each do |p|
      p.cafe_id = 0
      p.save
    end

    cafe = Cafe.find(params[:id])
    cafe.remove_image!
    cafe.remove_thumnail!
    cafe.destroy
    redirect_to '/'
  end
  
  def add_post
    if current_user
        @cafe = Cafe.where(:cafehash => params[:cafehash]).take
        post = Post.new
        post.user_id = current_user.id
        post.content = params[:postcontent]
        post.image = params[:postimage]
        post.address = @cafe.name
        post.cafe_id = @cafe.id
        post.writtentime = DateTime.now.in_time_zone(9).strftime("%Y-%m-%d %H:%M:%S")
        post.hashstr = SecureRandom.hex(11)
        post.score = params[:rating].to_i
        post.save
        count = params[:count].to_i
        for i in 0..count-1
          unless params[i.to_s] == ""
            tag_bucket = Tag.where(:content => params[i.to_s]).take
            if tag_bucket.nil?
              tag = Tag.new
              tag.content = params[i.to_s]
              tag.save
              @cafe.tags << tag
              post.tags << tag
            else
              if @cafe.tags.where(:content => tag_bucket.content).take.nil?
                caves_tags = CavesTag.new
                caves_tags.tag_id = tag_bucket.id
                caves_tags.cafe_id = @cafe.id
                caves_tags.save
              end
              post.tags << tag_bucket
            end
          end 
        end
        redirect_to(:back)
    else
        redirect_to "/users/sign_in"
    end
  end

  def add_reply
    @reply = Reply.new
    @reply.content=params[:reply]
    @reply.writtentime=DateTime.now.in_time_zone(9).strftime("%Y-%m-%d %H:%M:%S")
    @reply.user_id = current_user.id
    @reply.post_id = params[:post_id]
    @reply.save
    render :text =>@reply.to_json
  end
  
  def delete_post
    PostsTag.where(:post_id => params[:id]).take.destroy
    post = Post.find(params[:id])
    post.remove_image!
    post.destroy
    redirect_to(:back)
  end

  def delete_reply
    reply = Reply.where(:id=>params[:reply_id]).take
    reply.destroy
    render :text =>""
  end

  def edit_reply
    reply = Reply.where(:id=>params[:reply_id]).take
    reply.content = params[:reply]
    reply.writtentime = DateTime.now.in_time_zone(9).strftime("%Y-%m-%d %H:%M:%S")
    reply.save
    render :text => reply.to_json
  end

  def show_payinfo
    @user = User.where(id: current_user.id).take
    @cafe = Cafe.where(id: current_user.id).take
  end
  def test
    @user = User.all
    @post = Post.all
    @cafe = Cafe.all
    @tag = Tag.all  
  end
end
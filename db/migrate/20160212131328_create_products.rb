class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
    	t.string :product_name
    	t.integer :quantity
    	t.integer :stock
    	t.integer :price
    	t.string :order_message
    	t.string :cafe_id
    	t.string :user_id
    	t.string :payinfo_id

      t.timestamps null: false
    end
  end
end

class CreateCaves < ActiveRecord::Migration
  def change
    create_table :caves do |t|

	    t.string :name              , null: false, default: ""
      t.string :thumnail
      t.string :address
      t.string :address_road
      t.string :image
      t.string :cafecontact
      
      t.string :signaturemenu
      t.string :introduction
      t.string :cafehash          , null: false, default: ""
      
      t.integer :user_id
      t.float :latitude #위도
      t.float :longitude #경도

      t.boolean :paid             , null: false, default: false
      t.boolean :visible          , null: false, default: false
      
      t.string :webinfo_address       
      t.string :facebook_address      
      t.string :instagram_adderess    
      t.string :blog_address        
      t.string :machine 
      t.string :grinder 
      t.string :bean 
      t.string :brewing_method
      t.string :business_hour
      t.boolean :meal_available         , null: false, default: false
      t.boolean :parking                , null: false, default: false
      t.boolean :wifi                   , null: false, default: false
      t.boolean :sidemenu               , null: false, default: false
      t.boolean :animal_available       , null: false, default: false
      t.boolean :in_house_roasting      , null: false, default: false
      t.boolean :smoke                  , null: false, default: false
      t.boolean :today_business_check   , null: false, default: false

      t.string :machine_explanation
      t.string :machine_link
      t.string :grinder_explanation
      t.string :grinder_link
      t.string :bean_explanation
      t.string :bean_link
      t.string :brewing_method_explanation
      t.string :brewing_method_link
      t.timestamps null: false
    end
  end
end


class CreateCavesTags < ActiveRecord::Migration
  def change
    create_table :caves_tags do |t|
    	t.integer :cafe_id
    	t.integer :tag_id
      t.timestamps null: false
    end
  end
end

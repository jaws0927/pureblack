class CreateOrderinfos < ActiveRecord::Migration
  def change
    create_table :orderinfos do |t|
    	t.integer :user_id
    	t.string :contact
    	t.string :address
      	t.string :address_road
      t.timestamps null: false
    end
  end
end

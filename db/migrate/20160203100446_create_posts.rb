class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|

    	t.string :content              , null: false, default: ""
      t.string :image
      t.string :address              , null: false, default: "위치정보 없음"
      t.string :hashstr
      t.datetime :writtentime
      t.integer :user_id
      t.integer :cafe_id             , null: false, default: 0
      t.integer :score, default:0
      t.timestamps null: false
    end
  end
end

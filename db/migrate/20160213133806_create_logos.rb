class CreateLogos < ActiveRecord::Migration
  def change
    create_table :logos do |t|
      t.string :index
      t.string :other

      t.timestamps null: false
    end
  end
end

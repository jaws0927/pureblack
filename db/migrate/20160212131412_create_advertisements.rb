class CreateAdvertisements < ActiveRecord::Migration
  def change
    create_table :advertisements do |t|
    	t.string :type
    	t.string :term
    	t.integer :price
    	t.integer :cafe_id
    	t.integer :user_id
    	t.integer :payinfo_id
      t.timestamps null: false
    end
  end
end

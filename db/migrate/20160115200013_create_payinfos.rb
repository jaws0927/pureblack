class CreatePayinfos < ActiveRecord::Migration
  def change
    create_table :payinfos do |t|
	t.string :pay_method #PG사가 제공하는 결제수단('card' : 신용카드, 'trans' : 실시간계좌이체, 'vbank' : 가상계좌, 'phone' : 휴대폰소액결제)
	t.string :merchant_uid #상점에서 전달한 고유번호. 전달하지 않았을 때에는 임의 생성됨
	t.string :name #주문명
	t.integer :paid_amount #결제된 금액
	t.string :pg_provider #PG사 이름. inicis 등
	t.string :pg_tid #PG사 승인번호
	t.string :apply_num #카드사 승인번호(신용카드 결제일 때만 있음)
	t.string :vbank_num #가상계좌 입금계좌번호(가상계좌 결제일 때만 있음)
	t.string :vbank_name #가상계좌 은행명(가상계좌 결제일 때만 있음)
	t.string :vbank_holder #가상계좌 예금주(가상계좌 결제일 때만 있음)
	t.string :vbank_date #가상계좌 입금기한(가상계좌 결제일 때만 있음)
	t.integer :user_id
	t.integer :cafe_id
	t.string :pay_status
	t.datetime :paid_at

      t.timestamps null: false
    end
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160224115220) do

  create_table "advertisements", force: :cascade do |t|
    t.string   "type"
    t.string   "term"
    t.integer  "price"
    t.integer  "cafe_id"
    t.integer  "user_id"
    t.integer  "payinfo_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "caves", force: :cascade do |t|
    t.string   "name",                       default: "",    null: false
    t.string   "thumnail"
    t.string   "address"
    t.string   "address_road"
    t.string   "image"
    t.string   "cafecontact"
    t.string   "signaturemenu"
    t.string   "introduction"
    t.string   "cafehash",                   default: "",    null: false
    t.integer  "user_id"
    t.float    "latitude"
    t.float    "longitude"
    t.boolean  "paid",                       default: false, null: false
    t.boolean  "visible",                    default: false, null: false
    t.string   "webinfo_address"
    t.string   "facebook_address"
    t.string   "instagram_adderess"
    t.string   "blog_address"
    t.string   "machine"
    t.string   "grinder"
    t.string   "bean"
    t.string   "brewing_method"
    t.string   "business_hour"
    t.boolean  "meal_available",             default: false, null: false
    t.boolean  "parking",                    default: false, null: false
    t.boolean  "wifi",                       default: false, null: false
    t.boolean  "sidemenu",                   default: false, null: false
    t.boolean  "animal_available",           default: false, null: false
    t.boolean  "in_house_roasting",          default: false, null: false
    t.boolean  "smoke",                      default: false, null: false
    t.boolean  "today_business_check",       default: false, null: false
    t.string   "machine_explanation"
    t.string   "machine_link"
    t.string   "grinder_explanation"
    t.string   "grinder_link"
    t.string   "bean_explanation"
    t.string   "bean_link"
    t.string   "brewing_method_explanation"
    t.string   "brewing_method_link"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "caves_tags", force: :cascade do |t|
    t.integer  "cafe_id"
    t.integer  "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "follows", force: :cascade do |t|
    t.integer  "followable_id",                   null: false
    t.string   "followable_type",                 null: false
    t.integer  "follower_id",                     null: false
    t.string   "follower_type",                   null: false
    t.boolean  "blocked",         default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables"
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows"

  create_table "helps", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "likes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "logos", force: :cascade do |t|
    t.string   "index"
    t.string   "other"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orderinfos", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "contact"
    t.string   "address"
    t.string   "address_road"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "payinfos", force: :cascade do |t|
    t.string   "pay_method"
    t.string   "merchant_uid"
    t.string   "name"
    t.integer  "paid_amount"
    t.string   "pg_provider"
    t.string   "pg_tid"
    t.string   "apply_num"
    t.string   "vbank_num"
    t.string   "vbank_name"
    t.string   "vbank_holder"
    t.string   "vbank_date"
    t.integer  "user_id"
    t.integer  "cafe_id"
    t.string   "pay_status"
    t.datetime "paid_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string   "content",     default: "",        null: false
    t.string   "image"
    t.string   "address",     default: "위치정보 없음", null: false
    t.string   "hashstr"
    t.datetime "writtentime"
    t.integer  "user_id"
    t.integer  "cafe_id",     default: 0,         null: false
    t.integer  "score",       default: 0
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "posts_tags", force: :cascade do |t|
    t.integer  "post_id"
    t.integer  "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "product_name"
    t.integer  "quantity"
    t.integer  "stock"
    t.integer  "price"
    t.string   "order_message"
    t.string   "cafe_id"
    t.string   "user_id"
    t.string   "payinfo_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "replies", force: :cascade do |t|
    t.text     "content"
    t.datetime "writtentime"
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string   "content",    default: "", null: false
    t.integer  "counting",   default: 0,  null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "tags_users", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",                   default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "image"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "provider"
    t.string   "uid"
    t.string   "user_hash"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true

  create_table "users_products", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
